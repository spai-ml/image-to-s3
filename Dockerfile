
FROM tiangolo/meinheld-gunicorn:python3.7-alpine3.8

COPY ./app /app

RUN pip install -r /app/requirements.txt