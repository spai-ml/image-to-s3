import os
from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
from kafka import KafkaProducer
import werkzeug
import logging
from json import dumps
import s3

# Logger
logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter(
    '%(asctime)s - %(name)s - %(message)s'))
logger.addHandler(handler)
logger.setLevel(logging.INFO)

# Environment variable & Config
KAFKA_HOST = os.environ['KAFKA_HOST']
KAFKA_PORT = os.environ['KAFKA_PORT']
KAFKA_TOPIC_OUTPUT = os.environ['KAFKA_TOPIC_OUTPUT']
BUCKET_NAME = os.environ['S3_BUCKET']

# display environment variable
logger.info('KAFKA_HOST: {}'.format(KAFKA_HOST))
logger.info('KAFKA_PORT: {}'.format(KAFKA_PORT))
logger.info('KAFKA_TOPIC_OUTPUT: {}'.format(KAFKA_TOPIC_OUTPUT))
logger.info('S3_BUCKET: {}'.format(BUCKET_NAME))

app = Flask(__name__)
CORS(app)
api = Api(app)

producer = KafkaProducer(
    bootstrap_servers=['{}:{}'.format(KAFKA_HOST, KAFKA_PORT)])


class ImageAPI(Resource):
    def post(self):
        logger.info('/_api/image POST')
        parse = reqparse.RequestParser()
        # Field
        parse.add_argument(
            'picture', type=werkzeug.datastructures.FileStorage, location='files', required=True)
        parse.add_argument('pictureName', type=str, required=True)
        parse.add_argument('time', type=int, required=True)
        parse.add_argument('branch_id', type=int, required=True)
        parse.add_argument('camera_id', type=int, required=True)
        
        args = parse.parse_args()
        pictureFile = args['picture']
        pictureName = args['pictureName']
        time = args['time']
        branch_id = args['branch_id']
        camera_id = args['camera_id']
        logger.info('pictureName: {}'.format(pictureName))
        logger.info('time: {}'.format(time))
        logger.info('branch_id: {}'.format(branch_id))
        logger.info('camera_id: {}'.format(camera_id))

        # Upload to S3
        s3.upload_file(pictureFile, BUCKET_NAME, pictureName)
        logger.info('Upload "{}" to S3 Successfully'.format(pictureName))

        # Send S3 URI to Kafka
        s3_uri = "s3://{}/{}".format(BUCKET_NAME, pictureName)
        json = {
            "file_path": s3_uri,
            "branch_id": branch_id,
            "camera_id": camera_id,
            "time": time
        }
        producer.send(KAFKA_TOPIC_OUTPUT,
                      value=dumps(json).encode(encoding='UTF-8'))
        logger.info(dumps(json, indent=2))
        return None, 200


api.add_resource(ImageAPI, '/_api/image')

if __name__ == '__main__':
    app.run(port=5005)
