import boto3
from botocore import UNSIGNED
from botocore.client import Config
import os

# Config
S3_ENDPOINT = os.environ['S3_ENDPOINT']
S3_ACCESS_KEY = os.environ['S3_ACCESS_KEY']
S3_SECRET_KEY = os.environ['S3_SECRET_KEY']


def upload_file(file, bucket_name, file_key):
    s3 = boto3.resource('s3',
                        endpoint_url=S3_ENDPOINT,
                        aws_access_key_id=S3_ACCESS_KEY,
                        aws_secret_access_key=S3_SECRET_KEY,
                        config=Config(signature_version='s3v4'))
    img_bucket = s3.Bucket(bucket_name)
    img_bucket.upload_fileobj(file, file_key)
